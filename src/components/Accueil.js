import React from "react";

            
export class GetPictureAccueil extends React.Component{
    constructor(props) {
        super(props);
            this.state = {
                error : null,
                isLoaded: false,
                items: {},
                id: '',              
            };
            
        }

        componentDidMount(){
            fetch('https://api.themoviedb.org/3/movie/popular?api_key=fc6d0dcf0e2121d2ef56ded2ec39a655&language=fr-FR&page=1')
            .then(res => res.json())
            .then(
                (result)=>{
                    this.setState({
                        isLoaded: true,
                        items: result.results[0],
                        id: result.results[0].id
                    });                    
                },
                (error) => {
                    this.setState({
                        isLoaded : true,
                        error
                    });
                }
            )
        }


        render(){

            let image= 'https://image.tmdb.org/t/p/w500/'+ this.state.items.backdrop_path;
            let titre= this.state.items.title;
           
            const { error, isLoaded} = this.state;
            if(error){
                return <div> Erreur: {error.message}</div>
            }else if(!isLoaded){
                return <div>En cours de chargement...</div>
            }else{

            
                return(
                    <div>
                    <div id="container_accueil container-fluid text-center">
                        <div className="row">
                        <img className="col-8" id="image_accueil" src={image} alt="affiche de film"></img>                        
                        <h2 id="titre_image_accueil">{titre}</h2>  
                        </div>
                    </div>
                    </div>
                )
            }
        }
    }

    
    
    

     
    export default (GetPictureAccueil);