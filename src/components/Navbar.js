import React, {useState} from 'react';
import logo from '../images/logo-netflix.png';
import Search from './Search';
import Results from './Results';
import Popup from './DetailFilm';

const Navbar =()=>{ 

  const [state, setState] = useState({
    letter:"",
    results: [],
    selected: {}
  })

        
  const apiurl ="https://api.themoviedb.org/3/search/movie?api_key=fc6d0dcf0e2121d2ef56ded2ec39a655&language=fr-FR&page=1&include_adult=false&query=";
  const apiurlid="https://api.themoviedb.org/3/movie/";
  const apikey="?api_key=fc6d0dcf0e2121d2ef56ded2ec39a655&language=fr-FR";
        
  const search = (event) =>{
    if (event.key === "Enter"){
       fetch(apiurl + state.letter) 
      .then(res => res.json())     
      .then(
        (data)=>{
          
          let results = data.results;
          // console.log(results);
          setState(prevState =>{
            return {...prevState, results : results}
          })
        }
      );       
    }
  }
  
  const openPopup = (id)=>{
    fetch(apiurlid + id + apikey)
    .then(res => res.json())
    .then(
      (data)=>{
        let result = data;
        // console.log(result);
        setState(prevState=> {
          return{ ...prevState, selected : result}
        });

      });
  }

  const closePopup=()=>{
    setState(prevState=>{
      return{
        ...prevState, selected:{}
      }
    });
  }
  
  const handleInput = (event) =>{
          let letter =event.target.value;

          setState(prevState => {
            return {...prevState, letter : letter}
          });

        }
        let menu = ['FILMS'];        
        const listMenu = menu.map((nom, i) => <li className="nav-item active" key={ `nom_`+ i}><a className="nav-link">{nom}</a></li>)

        return(   
          <div>    
          <nav className="navbar navbar-expand-md">
          <img className="navbar-brand" src={logo} alt="logo netflix" />
          <div>Clone With MovieDb</div>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
          </button>

          <div classname="collapse navbar-collapse" id="navbarSupportedContent">

            <ul className="navbar-nav mr-auto">              
              {listMenu}
              <Search handleInput={handleInput} search={search}/>
            </ul>
            </div>
          </nav> 

          <section className="resultSearchFilm" >            
          <Results results={state.results} openPopup={openPopup} />
          {(typeof state.selected.title != "undefined") ? 
          <Popup selected={state.selected} closePopup={closePopup} /> : false}
          </section>
          </div>        
    )
}

export default Navbar;