import React from 'react';
import Result from './Result';

const Results =({results, openPopup}) =>{
       
    return (
        <div>
            
            <section className="result">
            
                {results.map(results => (
                <Result key={results.id} results={results} openPopup={openPopup}/>
                ))}
                
            </section>
        </div>
    )
}

export default Results;