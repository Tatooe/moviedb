
import React, {useState, useEffect} from 'react';

const Popup=({selected, closePopup})=>{
    const videoApi= `https://api.themoviedb.org/3/movie/`
    const finVideoApi=`/videos?api_key=fc6d0dcf0e2121d2ef56ded2ec39a655&language=fr-FR`;
    const youtube="https://www.youtube.com/embed/";
    const [state, setState] = useState([]);
    const [error, setError]=useState(false);
    
    useEffect( async () => {
        setError(false)
       
        fetch(videoApi + selected.id + finVideoApi)
        .then(res => res.json()  )
        .then ((data) =>{      
          let res = data.results[0];
          console.log(res)  
             setState(res)   
        },
        (error)=> {
            setError(true) 
            console.log(error)           
        })
      }, [])

      if (setError && state === undefined) {
          return (
              <>
            <section className="popup container_fluid ">
              <div className="content text-center p-2 m-auto">
                <div className="plot">
                  <div className="row">
            <img
              className="posterDetail col-12 col-md-2"
              src={`https://image.tmdb.org/t/p/w500/` + selected.poster_path}
              alt="poster"
            ></img>

              <div className="posterDetail_column col-12 col-md-8 text-left">               
                <h2>
                  {selected.title}
                  <span>({selected.release_date})</span>
                </h2>              
              <p className="rating">Note: {selected.vote_average}</p>              
              <p className="resume text-justify">{selected.overview}</p>              
              </div>
             </div>
          </div>

          <button className="close" onClick={closePopup}>
            FERMER
          </button>
        </div>
      </section>
            </>
          );
      }else{
    return (
        <>
            <section className="popup p-4 m-4">
              <div className="content  container_fluid  text-center">
                <div className="plot">
                  <div className="row align-items-center m-auto">
            <img
              className="posterDetail col-5 col-md-2 mx-auto mb-5"
              src={`https://image.tmdb.org/t/p/w500/` + selected.poster_path}
              alt="poster"
            ></img>

              <div className="posterDetail_column col-12 col-md-5 text-md-left">               
                <h2>
                  {selected.title}
                  <span>({selected.release_date})</span>
                </h2>              
              <p className="rating">Note: {selected.vote_average}</p>              
              <p className="resume text-justify">{selected.overview}</p>              
              </div>
              
               <iframe
                className="video col-12 col-md-4"
                width="390"
                height="250"
                src={youtube + state.key}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
              

            </div>
          </div>

          <button className="close my-4 mx-auto" onClick={closePopup}>
            FERMER
          </button>
        </div>
      </section>
      </>
    );
}
}

export default Popup;