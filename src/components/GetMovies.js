import React, { useEffect, useState } from "react";

import Results from "./Results";
import Popup from "./DetailFilm";

const GetMovies = (props) => {
  const [state, setState] = useState({
    results: [],
    selected: {},
  });
  const apiurl = "https://api.themoviedb.org/3/movie/";
  const page = "&page=1";
  const apiurlid = "https://api.themoviedb.org/3/movie/";
  const apikey = "?api_key=fc6d0dcf0e2121d2ef56ded2ec39a655&language=fr-FR";

  useEffect( async () => {
    fetch(apiurl + props.type + apikey +page)
    .then(res => res.json()  )
    .then ((data) =>{      
      let results = data.results;
      // console.log(results)
      setState(prevState=> {
        return{ ...prevState, results : results}
      });    
    })
  }, [])
 
   

  const openPopup =  (id) => {
     fetch(apiurlid + id + apikey)
    .then(res => res.json())
    .then(( data ) => {
      let result = data;
      // console.log(result);
      setState((prevState) => {
        return { ...prevState, selected: result };
      });
    });
  };

  const closePopup = () => {
    setState((prevState) => {
      return {
        ...prevState,
        selected: {},
      };
    });
  };

  return (
    
    <div className="carousel">       
      <Results results={state.results} openPopup={openPopup} />
      {typeof state.selected.title != "undefined" ? (
        <Popup selected={state.selected} closePopup={closePopup} />
      ) : (
        false
      )}
      ;
    </div>
  );
};

export default GetMovies;