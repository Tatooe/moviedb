import React from 'react';

const Result =({results, openPopup}) =>{
    return (  
        <div>
            
        <div className="result" onClick={()=> openPopup(results.id)}>
            
            <div className="poster_title text-center">
            <img src={`https://image.tmdb.org/t/p/w500/`+ results.poster_path} alt=""/>
            <h3>{results.title}</h3>
            </div>
        </div>
        </div>
    )
}

export default Result;