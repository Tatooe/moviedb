import React from 'react';

const Search =({handleInput, search}) =>{
    return(
        <section className="containerSearchBox form-inline" >
            <input type="text" placeholder="Rechercher un film..." className="searchBox form-control " 
            onChange={handleInput}
            onKeyPress={search}/>            
        </section>
    )
}

export default Search;