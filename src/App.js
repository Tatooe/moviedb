import React from 'react';
import Navbar from './components/Navbar';
import { GetPictureAccueil } from './components/Accueil';
import GetMovies from './components/GetMovies';



function App() {

  return (
    <>

    <header className="App-header">
    <div className="App">
    <Navbar />     
    </div>
    </header>

       
      <section id="container_movie" className="text-center text-md-left">
    <GetPictureAccueil />        

    <h2  className=" mt-5">FILMS DU MOMENT</h2>
    <GetMovies type='now_playing'/>

    <h2>FILMS A VENIR</h2>
    <GetMovies type='upcoming'/>

    <h2>FILMS POPULAIRES</h2>
    <GetMovies type='popular'/>


    <h2>FILMS LES MIEUX NOTES</h2>
    <GetMovies type='top_rated'/>     
      </section>
    
    
    </>
  );
}

export default App;